//
//  StudentViewController.swift
//  Laba1
//
//  Created by Admin on 11.02.16.
//  Copyright © 2016 Admin. All rights reserved.
//


import UIKit

class StudentViewController : UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var studentTable: UITableView!
    
    let group:[[String]] = [["Balashko","Grushevskiy", "Dvorko", "Elinskiy", "Zykov", "Ivanov", "Kozorez", "Krizhanovskiy", "Lazaryonok", "Litvin", "Litvinchuk", "Lukin", "Molotova", "Nehay", "Nickolaeva", "Polischuk", "Rozum", "Safonov", "Sivakov", "Stasukevich", "Tapunov", "Fedorenko", "Fyodorov", "Tselischeva", "Tsivako", "Chechyotko", "Shaveyko", "Shevchuk", "Shishov", "Scherbina"],["Grodno", "Baranovichi", "Minsk", "Ivatsevichi", "Minsk", "Pinsk","Mogilyov", "Minsk", "Govel","Zelva", "Volkovysk", "Grodno","Brest", "Vitebsk", "Grodno","Minsk", "Bobruysk", "Borisov","Stolbtsy", "Minsk", "Grodo","Gomel", "Baranovichi", "Minsk","Mozyr", "Vitebsk", "Mogilyov","Volkovysk", "Zelva", "Minsk"]]
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group[0].count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("studentTableCell")
        cell?.textLabel?.text = group[0][indexPath.row]
        cell?.detailTextLabel?.text = group[1][indexPath.row]
        return cell!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}