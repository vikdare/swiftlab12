//
//  ViewController.swift
//  Laba1
//
//  Created by Admin on 11.02.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var LitersInput: UITextField!
    @IBOutlet weak var LaufLabel: UILabel!
    @IBOutlet weak var GalloneLabel: UILabel!
    @IBOutlet weak var PintLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func ConverBtnClicked(sender: AnyObject) {
        let pintConst = 2.113
        let gallonesConst = 0.2642
        let laufsConst = 0.00629
        
        if let liters = Double(LitersInput.text!) {
            PintLabel.text = PintLabel.text! + " " + String(Double(liters) * pintConst)
            GalloneLabel.text = GalloneLabel.text! + " " + String(Double(liters) * gallonesConst)
            LaufLabel.text = LaufLabel.text! + " " + String(Double(liters)*laufsConst)
        }
    }

}

