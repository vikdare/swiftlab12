//
//  WebViewController.swift
//  Laba1
//
//  Created by Admin on 11.02.16.
//  Copyright © 2016 Admin. All rights reserved.
//

import UIKit

class WebViewController : UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    let urlPath = NSURL (string: "https://www.onetwotrip.com");
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let request = NSURLRequest(URL: urlPath!)
        webView.loadRequest(request)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}